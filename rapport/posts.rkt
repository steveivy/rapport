#lang racket

(require racket/path racket/date yaml markdown gregor)

(provide make-post string->post path->post (struct-out post))

(define split-content
  (λ (content)
    (if (string-contains? content "\n---\n")
        (string-split content #rx"\n---\n")
        (string-split content #rx"\n\n" )
        )))

(module+ test
  (require rackunit)
  (check-equal? (split-content "foo\n---\nbar") '("foo" "bar"))
  (check-equal? (split-content "foo\n\nbar") '("foo" "bar"))
  )

(define parse-frontmatter
  (λ (frontmatter-string)
    (string->yaml frontmatter-string)))


(define parse-content
  ;; returns an html xexpr
  (λ (content)
    (parse-markdown content)))

(struct post (id
              title
              slug
              post-date
              tags
              frontmatter-src
              body-src) #:mutable #:transparent)

(define/contract date->postdate
  (-> date? datetime?)
  (λ (date-val)
    (datetime
     (->year date-val)
     (->month date-val)
     (->day date-val)
     0 0 0)))

(define/contract date*->postdate
  (-> date*? datetime?)
  (λ (date-val)
    (posix->datetime (date*->seconds date-val))))


(define/contract string->postdate
  (-> string? datetime?)
  (λ (date-val
      #:default [default date-val])
    ; (writeln (format "parse-post-date ~a ~a" date-string))
    (cond
      [(regexp-match #px"^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$" date-val)
       ; (displayln (format "parse ~a with yyyy-MM-dd'T'HH:mm:ss'Z'" date-string))
       (parse-datetime date-val "yyyy-MM-dd'T'HH:mm:ss'Z'")]
      [(regexp-match #px"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}$" date-val)
       ; (displayln (format "parse ~a with yyyy-MM-dd HH:mm" date-val))
       (parse-datetime date-val "yyyy-MM-dd HH:mm")]
      [else #f])))

(module+ test
  (check-equal? (string->postdate "2002-12-23T00:00:00Z") (datetime 2002 12 23))
  (check-equal? (string->postdate "2002-12-23 00:00") (datetime 2002 12 23))
  (check-equal? (date->postdate (date 2002 12 23)) (datetime 2002 12 23))

  ; (define fm (string->yaml "date: 2002-12-23T00:00:00Z"))
  ; (displayln (hash-ref fm "date"))
  ; (displayln (parse-post-date (hash-ref fm "date")))
  ; (check-equal? (parse-post-date (hash-ref fm "date"))
  ;               (->datetime/utc (datetime 2002 12 23 0 0 0)))
  )

(define hash-clean
  (λ (h)
    (hash-map/copy h
                   (λ (k v)
                     (values k (cond
                                 [(string? v)
                                  (string-trim v)]
                                 [(eq? #f v)
                                  ""]

                                 [else v]))))))

(module+ test
  (check-equal? (hash "k1" "") (hash-clean (hash "k1" #f))))

(define make-post
  (λ (#:id [id #f]
      #:title [title ""]
      #:body-src [body-src ""]
      #:slug [slug ""]
      #:tags [tags ""]
      #:postdate [post-date ""]
      #:frontmatter-src [frontmatter-src ""])

    (displayln (format "title: ~a" title))
    (displayln (format "slug: ~a" slug))
    (displayln (format "tags: ~a" tags))
    (displayln (format "date: ~a" post-date))

    (define dt (datetime->iso8601
                (cond
                  [(datetime? post-date) post-date]
                  [(date*? post-date)
                   (date*->postdate post-date)]
                  [(date? post-date)
                   (date->postdate post-date)]
                  [(string? post-date)
                   (string->postdate post-date)]
                  )))
    (displayln "fm src:\n------")
    (displayln frontmatter-src)

    (define fm
      (if (eq? frontmatter-src "")
          (hasheq 'title title
                  'slug slug
                  'date post-date
                  'tags tags)
          (string->yaml frontmatter-src)))
    (displayln "fm:\n------")
    (displayln fm)
    (when (not (hash-has-key? fm "date"))
      (hash-set! fm "date" dt))

    (post id
          title
          slug
          post-date
          tags
          (yaml->string (hash-clean fm))
          body-src)))

;; custom accessors that parse strings into native datatypes
;; return body as parsed markdown (listof xexpr?)
(define post-body
  (λ (post)
    (parse-markdown (post-body-src post))))

;; return fronmatter as parsed yaml (hash?)
(define post-frontmatter
  (λ (post)
    (string->yaml (post-frontmatter-src post))))

(define string->post
  (λ (file-name post-string)
    (define file-slug (first
                       (string-split file-name ".")))

    (match-define (list m year month day slug)
      (regexp-match #px"^(\\d{4})-(\\d{2})-(\\d{2})-(.*)" file-slug))

    (match-define (list frontmatter-string content)
      (split-content post-string))

    (define frontmatter (parse-frontmatter frontmatter-string))

    (make-post
     #:title (hash-ref frontmatter "title"
                       (string-titlecase
                        (string-join (string-split file-slug "-") " ")))
     #:slug (if (hash-has-key? frontmatter "slug")
                (hash-ref frontmatter "slug")
                slug)
     #:postdate (if (not (hash-has-key? frontmatter "date"))
                    ;; construct a datetime from the path
                    (datetime (string->number year)
                              (string->number month)
                              (string->number day)
                              0 0 0)
                    (hash-ref frontmatter "date"))
     #:tags (hash-ref frontmatter "tags" "")
     #:frontmatter-src frontmatter-string
     #:body-src content)))

(module+ test
  (define test-fm-string "title: foo")
  (define post-string "title: foo\n---\nbody")
  (define expect-post (make-post #:title "foo"
                                 #:slug "foo"
                                 #:postdate (datetime 2002 12 23 0 0 0)
                                 #:frontmatter-src test-fm-string
                                 #:body-src "body"))

  (check-equal? (string->post "2002-12-23-foo" post-string) expect-post)
  )

(define path->post
  (λ (path)
    ;; build post from file contents
    (define file-name (path->string(file-name-from-path path)))
    (define file-content (file->string path))

    (string->post file-name file-content)))

;(module+ main
;  (define p "/Users/sivy/projects/monkinetic-content/posts/2002-05-27-south-mountain-redux.md")
;  (string->post (path->string (file-name-from-path p)) (file->string p))
;  (define p1 (string->post (path->string (file-name-from-path p)) (file->string p)))
;  (post-title p1)
;  (post-frontmatter-src p1)
;  (post-frontmatter p1)
;  (post-body-src p1)
;  (post-body p1))