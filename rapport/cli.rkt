#lang racket

(require racket/base racket/cmdline file/glob)
(require "files.rkt" "posts.rkt" "storage.rkt" "indexer.rkt" "paths.rkt" "web.rkt")

(require dotenv)

(define verbose-mode (make-parameter #f))
(define db-file (make-parameter ""))

(define run-cli
  (λ ()
    (command-line
     #:program "rapport"
     #:once-each
     [("-v" "--verbose") "Compile with verbose messages"
                         (verbose-mode #t)]
     [("--db") f
               "SQLite database file, required for --create-db, --index, --search, --run"
               (db-file f)]

     [("-b" "--base-path") path
                           "Base path to a Rapport site"
                           (when (not (absolute-path? path))
                               (raise-argument-error 'base-path "absolute-path?" path))
                             (site-base path)]
     [("--list") dir
                 (""
                  "List markdown files in content directory")
                 (for ([f (glob (string-append dir
                                               "/" "*.md"
                                               ))])
                   (displayln (path->string f)))]

     [("--create-db")
      "Create the db file"
      (define slc (db-connect (db-file)))
      (displayln "Connected, creating db schema")
      (create-db-schema slc)
      (displayln "...done")
      ]

     [("--index") dir
                  "Read content into the db"
                  (define slc (db-connect (db-file)))
                  (displayln (format "Connected, indexing directory ~a" dir))
                  (index-posts slc dir)
                  (displayln "...done")]
     
     [("--run" "--run-server")
      "Run the application server"
      (serve-http)]
     ; [("-s" "--search")
     ;  "Search the local database"
     ;  (get-posts)]
     )))

(module+ main
  (run-cli)
  )