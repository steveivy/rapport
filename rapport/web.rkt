#lang racket

(require routy)

(require web-server/servlet
         web-server/servlet-env
         web-server/templates)

(require response-ext)
(require "storage.rkt" "posts.rkt" "paths.rkt")

(provide serve-http)

; (define render-index
;  (include-template "index.html"))

(routy/get "/test"
           (λ (req params)
             
             (define posts (get-posts (db-connect ) #:limit 10))
             (let ([posts (get-posts)])
               ; (render-index)
               (for/list ([p posts])
                 (displayln))
               )))

(routy/get "/about"
           (λ (req params)
             "About page"))

(routy/get "/:year/:month/:day/:slug"
           (λ (req params)
             (format "~a / ~a / ~a / ~a"
                     (request/param params 'year)
                     (request/param params 'month)
                     (request/param params 'day)
                     (request/param params 'slug))))

(define tmp-templates-dir (make-parameter ""))

(define server-startup
  (λ ()
    (displayln "Server Startup...")
    (displayln (format "cwd: ~a" (current-directory)))
    (define tmp-dir "../tmp")
    (unless (directory-exists? tmp-dir)
      (make-directory tmp-dir))
    
    (tmp-templates-dir (make-temporary-directory #:base-dir tmp-dir))
    (displayln (format "Templates src directory: ~a" (tmp-templates-dir)))
    (displayln (format "Temp templates directory: ~a" (tmp-templates-dir)))
    (copy-directory/files (site-templates-path) (tmp-templates-dir))))

(define server-shutdown
  (λ ()
    (displayln "Server Shutdown...")
    (displayln (format "Removing temp templates directory: ~a" (tmp-templates-dir)))
    (delete-directory tmp-templates-dir)))

(define serve-http
  (λ (#:launch-browser? [open-browser #f]
      #:port [port 8001])
    ;; startup tasks
    ;; (server-startup)
    ;; 
    (serve/servlet
     (λ (req) (routy/response req))
     #:port port     
     #:launch-browser? open-browser
     #:servlet-path ""
     #:servlet-regexp #rx"")
    ;; startup tasks
    ; (server-shutdown)
    ))
     

(module+ main
  (serve-http)
  )